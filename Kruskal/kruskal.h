#ifndef _KRUSKAL_H
#define _KRUSKAL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <string.h>

#define MAXIMOvert 200

typedef struct Aresta{
	int vertO;
	int vertD;
	int peso;
	struct Aresta* proximo;
} Aresta;

typedef struct Grafo{
    int ponderado;
    int digrafo;
    int matriz[MAXIMOvert][MAXIMOvert];
    Aresta *arestas;
}Grafo;

void acrescentar_Grafo(Grafo *gr, int vertO, int vertD, int peso);
void kruskal(Grafo *gr, Grafo *grafoFinal);
void printar_Grafo(Grafo *gr);
int calcularPesoGrafo(Grafo *gr);
void entradaFinal(Grafo *gr, char arquivo[]);
void saidaFinal(Grafo *gr, FILE *arquivo);
Aresta* Montar_Aresta();
void bubbleSort(Aresta* l);
void bubbleSortAresta(Aresta* l);
Aresta* acrescentar_Aresta(Aresta* l, int vertO, int vertD, int peso);
Grafo *montar_Grafo(int ponderado, int digrafo);


#endif