#include "kruskal.h"

int main(int argv, char *argc[]){
	Grafo *grafoI = montar_Grafo(0, 1);
    Grafo *grafoF = montar_Grafo(0, 1);
    int i;
    int flag = 0;
    for(i = 1; i < argv; i++){
        if(strcmp(argc[i], "-h") == 0){
            printf("\n-h                    - ajuda\n");
            printf("\n-f arquivo.dat -  entrada\n");
            printf("\n-o arquivo.dat -  saida\n" );
            printf("\n-s inteiro     -  solu��o\n");
        }
 if(strcmp(argc[i], "-f")== 0){
        entradaFinal(grafoI, argc[i+1]);
        Kruskal(grafoI, grafoF);
        flag = 1;
        } 
if(strcmp(argc[i], "-o") == 0){
        FILE *arquivo = fopen(argc[i+1], "w");
        if (arquivo == NULL){
        	printf("ERRO!!!! - arquiv0 invalido\n");
        	}
        else{
        	saidaFinal(grafoF, arquivo);
        	}
            fclose(arquivo); 
            flag = 1;
        }
if(strcmp(argc[i], "-s")== 0){
            printar_Grafo(grafoF);       
            flag = 0;
        } 
    }
if (flag == 1){
    Aresta *temporario = grafoF->arestas;
    int peso = 0;
    while(temporario != NULL){
        peso += temporario -> peso;
            temporario= temporario->proximo;
        }
        printf("%d\n", peso);
    }
    return 0;
}

