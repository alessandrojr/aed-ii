#include "kruskal.h"

Aresta* Montar_Aresta(){
	return NULL;
}

void kruskal(Grafo *gr, Grafo *grafoF){
    bubbleSort(gr->arestas);
    Aresta *temporario;
    int vetorAuxiliar[MAXIMOvert];
    int peso, peso1, peso2;
    int i;
    for(i = 0; i < MAXIMOvert; i++){
        vetorAuxiliar[i] = i;}
    peso = 0;
    for(temporario = gr->arestas; temporario != NULL; temporario = temporario->proximo)
    {
        if(vetorAuxiliar[temporario->vertO] != vetorAuxiliar[temporario->vertD]){
            peso1 = vetorAuxiliar[temporario->vertO];
            peso2 = vetorAuxiliar[temporario->vertD];
        for(int j = 0; j < MAXIMOvert; j++){
            if(vetorAuxiliar[j] == peso2){
                vetorAuxiliar[j] = peso1;}}
        acrescentar_Grafo(grafoF, temporario->vertO, temporario->vertD, temporario->peso);
        peso = peso + temporario->peso;}}}

void bubbleSort(Aresta* l){
	Aresta* auxiliar;
	int trocou = 0;	
	int temporarioV1, temporarioV2, temporarioP;
	while(trocou == 0){
		trocou = 1;
		for(auxiliar = l; auxiliar->proximo!=NULL; auxiliar = auxiliar->proximo){
			if(auxiliar->peso > auxiliar->proximo->peso){
				temporarioP = auxiliar->peso;
				auxiliar->peso = auxiliar->proximo->peso;
				auxiliar->proximo->peso = temporarioP;
				temporarioV1 = auxiliar->vertO;
				auxiliar->vertO = auxiliar->proximo->vertO;
				auxiliar->proximo->vertO = temporarioV1;	
				temporarioV2 = auxiliar->vertD;
				auxiliar->vertD = auxiliar->proximo->vertD;
				auxiliar->proximo->vertD = temporarioV2;
				trocou = 0;}}}}


void bubbleSortAresta(Aresta* l){
	Aresta* auxiliar;
	int trocou = 0;	
	int temporarioV1, temporarioV2, temporarioP;
	while(trocou == 0){
		trocou = 1;
		for(auxiliar = l; auxiliar->proximo!=NULL; auxiliar = auxiliar->proximo){
			if(auxiliar->vertO > auxiliar->proximo->vertO){
				temporarioP = auxiliar->peso;
				auxiliar->peso = auxiliar->proximo->peso;
				auxiliar->proximo->peso = temporarioP;
				
				temporarioV1 = auxiliar->vertO;
				auxiliar->vertO = auxiliar->proximo->vertO;
				auxiliar->proximo->vertO = temporarioV1;
				
				temporarioV2 = auxiliar->vertD;
				auxiliar->vertD = auxiliar->proximo->vertD;
				auxiliar->proximo->vertD = temporarioV2;
				trocou = 0;}}}}


Aresta* acrescentar_Aresta(Aresta* l, int vertO, int vertD, int peso){
	Aresta* novaAresta = (Aresta*)malloc(sizeof(Aresta));
	novaAresta->vertO = vertO;
	novaAresta->vertD = vertD;
	novaAresta->peso = peso;
	if(l==NULL){
		novaAresta->proximo = NULL;
		return novaAresta;
	}
	novaAresta->proximo = l;
	return novaAresta;
}


Grafo *Montar_Grafo(int ponderado, int digrafo)
{
    Grafo *gr = (Grafo*)malloc(sizeof(Grafo));
    Aresta *arestas = (Aresta*)malloc(sizeof(Aresta));
    gr->arestas = arestas;
    int i, j;
    for(i = 0; i < MAXIMOvert; i++){
        for(j = 0; j < MAXIMOvert; j++){
            gr->matriz[i][j] = 0;
        }
    }
    gr->digrafo = digrafo;
    gr->ponderado = ponderado;

    
    return gr;
}


void acrescentar_Grafo(Grafo *gr, int vertO, int vertD, int peso)
{
    gr->arestas = acrescentar_Aresta(gr->arestas, vertO, vertD, peso);

    if(gr->ponderado == 0 && gr->digrafo == 0)
    {
        gr->matriz[vertO][vertD] = peso;
    }

    else if(gr->ponderado == 0 && gr->digrafo == 0)
    {
        gr->matriz[vertO][vertD] = peso;
        gr->matriz[vertD][vertO] = peso;
    }

    else if(gr->ponderado == 0 && gr->digrafo == 0)
    {
        gr->matriz[vertO][vertD] = 1;
    }

    else if(gr->ponderado == 0 && gr->digrafo == 0)
    {
        gr->matriz[vertO][vertD] = 1;
        gr->matriz[vertD][vertO] = 1;}}



void printar_Grafo(Grafo *gr){
	bubbleSortAresta(gr->arestas);
	Aresta *temporario = gr->arestas -> proximo;
	while(temporario != NULL){
		printf("(%d,%d) ", temporario->vertO, temporario->vertD);
		temporario= temporario->proximo;
	}
	printf("\n");
}

int calcularPesoGrafo(Grafo *gr){
	Aresta *temporario = gr->arestas -> proximo;
	int peso = 0;
	while(temporario != NULL){
		peso += temporario -> peso;
		temporario= temporario->proximo;
	}
	return peso;
}

void entradaFinal(Grafo *gr, char arquivo[]){
    FILE *abrirArquivo = fopen(arquivo, "r");
    if(abrirArquivo == NULL){
        printf("erro abrir arquivo entrada\n");
        exit(0);
    }
    else{
        int verts, arestas;
        fscanf(abrirArquivo, "%d %d\n", &verts, &arestas);
        char entrada[10];
        int i;
        for(i = 0; i < arestas; i++){
            int origem, destino, peso;
            fscanf(abrirArquivo, "%d %d %d\n", &origem, &destino, &peso);
            acrescentar_Grafo(gr, origem, destino, peso);
       
        }

    }
    fclose(abrirArquivo);            
}



void saidaFinal(Grafo *gr, FILE *arquivo){
	bubbleSortAresta(gr->arestas);
	Aresta *temporario = gr->arestas->proximo;   
	while(temporario != NULL){
		fprintf(arquivo, "(%d,%d) ", temporario->vertO, temporario->vertD);
		temporario= temporario->proximo;
	}
}
