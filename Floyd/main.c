#include "floyd.h"


int main(int argv, char *argc[]){
	Grafo *grafoI = montar_Grafo(true, true);
    Grafo *grafoF = montar_Grafo(true, true);
    int i;
    int flag = 0;
    int flagPrintEspecifico = 0;
    int vertInicial = 0;
    int vertFinal = 0;
    int flagSalvar = 0;
    char arquivoSaida[100];

    for(i = 1; i < argv; i++){
        if(strcmp(argc[i], "-h") == 0){
            printf("\n-h                    -  ajuda\n");
            printf("\n-f arquivo.dat -  entrada\n");
            printf("\n-o arquivo.dat -  saida\n" );
            printf("\n-i nteiro     -  v�rtice inicial \n");
            printf("\n-l inteiro     -  v�rtice final\n");

        }
        if(strcmp(argc[i], "-f")== 0){
            entradaFinal(grafoI, argc[i+1]);
            flag = 1;
        } 
        if(strcmp(argc[i], "-i")== 0){
            floydWarshall(grafoI, grafoF);
            vertInicial = atoi(argc[i+1]);

            flag = 1;
        } 
        if(strcmp(argc[i], "-o") == 0){
            strcpy(arquivoSaida, argc[i+1]);
        	flagSalvar = 1;
            flag = 1;
        }
       
    if (flagPrintEspecifico == 0){
       
        imprimir(grafoF, vertInicial);
    }
    if(flagSalvar == 1){
        FILE *arquivo = fopen(arquivoSaida, "w");
        if (arquivo == NULL){
            printf("ERRO - Arquivo de saida invalido\n");
        }
        else{
            saidaFinal(grafoF, vertInicial, arquivo);
        }
        fclose(arquivo); 
    }
    return 0;
}
}  
