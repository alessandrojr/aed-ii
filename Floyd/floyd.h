#ifndef __Djikstra__
#define __Djikstra__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VerticeMaximo 50
#define ARESTAMAX 20
#define true 1
#define false 0

typedef struct lista{
	int vert1;
	int vert2;
	int peso;
	struct lista* proximo;
} Lista;

typedef struct __GRAFO__
{
    int ponderado;
    int digrafo;
    int matriz[VerticeMaximo][VerticeMaximo];
    Lista *arestas;
}Grafo;

typedef struct listaAuxiliar
{
    int vert;
    int custo;
    int prev;
}ListaAuxiliar;

void floydWarshall(Grafo *gr, Grafo *grafoFinal);
int numero_Vertice(Grafo *gr);
void imprimir(Grafo *gr, int vertInicial);
void entradaFinal(Grafo *gr, char arquivo[]);
void saidaFinal(Grafo *gr, int vertInicial, FILE *arquivo);
Lista* montar_Lista();
void bubbleSort(Lista* l);
Lista* acrescentar_Lista(Lista* l, int vert1, int vert2, int peso);
Grafo *montar_Grafo(int ponderado, int digrafo);
void acrescentar_Grafo(Grafo *gr, int vert1, int vert2, int peso);

#endif