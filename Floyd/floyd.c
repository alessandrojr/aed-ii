#include "floyd.h"
#include "Heap.c"

Lista* montar_Lista(){
	return NULL;
}
Grafo *montar_Grafo(int ponderado, int digrafo)
{
    Grafo *grafo = (Grafo*)malloc(sizeof(Grafo));
    Lista *arestas = (Lista*)malloc(sizeof(Lista));
    grafo->arestas = arestas;
    int i, j;
    for(i = 0; i < VerticeMaximo; i++){
        for(j = 0; j < VerticeMaximo; j++){
            grafo->matriz[i][j] = 0;
        }
    }
    grafo->digrafo = digrafo;
    grafo->ponderado = ponderado;

    
    return grafo;
}

void bubbleSort(Lista* l){
	Lista* Auxiliar;
	int trocou = 0;	
	int tempV1, tempV2, tempP;
	while(trocou == 0){
		trocou = 1;
		for(Auxiliar = l; Auxiliar->proximo!=NULL; Auxiliar = Auxiliar->proximo){
			if(Auxiliar->peso > Auxiliar->proximo->peso){
				tempP = Auxiliar->peso;
				Auxiliar->peso = Auxiliar->proximo->peso;
				Auxiliar->proximo->peso = tempP;
				
				tempV1 = Auxiliar->vert1;
				Auxiliar->vert1 = Auxiliar->proximo->vert1;
				Auxiliar->proximo->vert1 = tempV1;
				
				tempV2 = Auxiliar->vert2;
				Auxiliar->vert2 = Auxiliar->proximo->vert2;
				Auxiliar->proximo->vert2 = tempV2;
				trocou = 0;
			}	
		}
	}
}

Lista* acrescentar_Lista(Lista* l, int vert1, int vert2, int peso){
	Lista* novo = (Lista*)malloc(sizeof(Lista));
	novo->vert1 = vert1;
	novo->vert2 = vert2;
	novo->peso = peso;
	if(l==NULL){
		novo->proximo = NULL;
		return novo;
	}
	novo->proximo = l;
	return novo;
}



void acrescentar_Grafo(Grafo *grafo, int vert1, int vert2, int peso)
{
    grafo->arestas = acrescentar_Lista(grafo->arestas, vert1, vert2, peso);

    if(grafo->ponderado == true && grafo->digrafo == true)
    {
        grafo->matriz[vert1][vert2] = peso;
    }

    else if(grafo->ponderado == true && grafo->digrafo == false)
    {
        grafo->matriz[vert1][vert2] = peso;
        grafo->matriz[vert2][vert1] = peso;
    }

    else if(grafo->ponderado == false && grafo->digrafo == true)
    {
        grafo->matriz[vert1][vert2] = 1;
    }

    else if(grafo->ponderado == false && grafo->digrafo == false)
    {
        grafo->matriz[vert1][vert2] = 1;
        grafo->matriz[vert2][vert1] = 1;
    }
}



void floydWarshall(Grafo *grafo, Grafo *grafoFinal)
{
    int floyd[VerticeMaximo][VerticeMaximo];
    int i, j, k;
    for(i = 0; i < VerticeMaximo; i++){
        for(j = 0; j < VerticeMaximo; j++){
            floyd[i][j] = grafo->matriz[i][j];
            if(floyd[i][j] == 0)
            {
                floyd[i][j] = 100000;}}}
    for(k = 0; k < VerticeMaximo; k++){
        for(i = 0; i < VerticeMaximo; i++){
            for(j = 0; j < VerticeMaximo; j++){
                if(i == j){
                    floyd[i][j] = 0;}
                else if(floyd[i][k] + floyd[k][j] < floyd[i][j])
                {
                    floyd[i][j] = floyd[i][k] + floyd[k][j];}}}}
   
    for(i = 1; i <= (numero_Vertice(grafo)); i++ )
    {
        for(j = 1; j <=(numero_Vertice(grafo)); j++)
        {
            if(floyd[i][j] != 10000)
            {
                acrescentar_Grafo(grafoFinal, i, j, floyd[i][j]);}}}}


int numero_Vertice(Grafo *grafo)
{
    int maior;
    maior = grafo->arestas->vert1;
    Lista *temp;

    for(temp = grafo->arestas; temp != NULL; temp = temp->proximo)
    {
        if(temp->vert1 > maior)
        {
            maior = temp->vert1;
        }
        else if(temp->vert2 > maior)
        {
            maior = temp->vert2;
        }
    }
    return maior;
}


void imprimir(Grafo *grafo, int vertInicial)
{
    int i, j;
    printf("(%d:%d) ", vertInicial, 0);
        for(j = 0; j < VerticeMaximo; j++){
            if(grafo->matriz[vertInicial][j] != 0 && grafo->ponderado == false)
            {
                if(vertInicial != j){
                    printf("(%d:%d) ",j, grafo->matriz[vertInicial][j]);}}
            else if(grafo->matriz[vertInicial][j] != 0 && grafo->ponderado == true)
            {   
                if (vertInicial != j)
                 {
                     printf("(%d:%d) " ,j,grafo->matriz[vertInicial][j]);}}}
    printf("\n");
    
}

void entradaFinal(Grafo *grafo, char arquivo[]){
    FILE *abrirArquivo = fopen(arquivo, "r");
    if(abrirArquivo == NULL){
        printf("erro abrir arquivo entrada\n");
        exit(0);
    }
    else{
        int verts, arestas;
        fscanf(abrirArquivo, "%d %d\n", &verts, &arestas);
        char entrada[10];
        int i;
        for(i = 0; i < arestas; i++){
            int origem, destino, peso;
            fscanf(abrirArquivo, "%d %d %d\n", &origem, &destino, &peso);
            acrescentar_Grafo(grafo, origem, destino, peso);
       
        }

    }
    fclose(abrirArquivo);            
}



void saidaFinal(Grafo *grafo, int vertInicial, FILE *arquivo){
    int i, j;
    fprintf(arquivo, "(%d:%d) ", vertInicial, 0);
        for(j = 0; j < VerticeMaximo; j++)
        {
            if(grafo->matriz[vertInicial][j] != 0 && grafo->ponderado == false)
            {
                if(vertInicial != j){

                    fprintf(arquivo, "(%d:%d) ",j, grafo->matriz[vertInicial][j]);
                }
            }
            else if(grafo->matriz[vertInicial][j] != 0 && grafo->ponderado == true)
            {   
                if (vertInicial != j)
                 {
                     fprintf(arquivo, "(%d:%d) " ,j,grafo->matriz[vertInicial][j]);
                 } 
            }
        }
    fprintf(arquivo, "\n");
}
