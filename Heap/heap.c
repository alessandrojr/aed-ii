#include "heap.h"



void init(heap *h, int t){
   h->a = (int* )malloc(sizeof(int)*(t+1));
   h->tam = 0;
   h->comp = t;
   
}

int adicionar(heap *h, int valor){
   if(h->tam == h->comp){
      return -1;
   }
   h->tam++;
   h->a[h->tam] = valor;
	return 0;
}
void trocar(heap *h, int i, int  maior){
   int aux;
   aux = h->a[maior];
   h->a[maior] = h->a[i];
   h->a[i] = aux;
  
}
void maxHeapify(heap *h, int i){
   int l, r, maior;
   l = esquerdo(i);
   r = direito(i);
   
   if(l <= h->tam && h->a[l] < h->a[i]){
      maior = l;
   }else{
      maior = i;
   }
   if(r <= h->tam && h->a[r] < h->a[maior]){
      maior = r;
   }
   if(maior != i){
      trocar(h, i, maior);
      maxHeapify(h, maior);
   }
}

void CriarMaxHeap(heap *h){
   int i;
   
   h->tam = h->comp;
   
   for(i = (h->comp/2); i>0; i--){
      maxHeapify(h, i);
   }
   
}

void maxHeapSort(heap *h){
   int i, tam;
   tam = h->tam;
   
   CriarMaxHeap(h);
   
   for(i = h->comp; i>1; i--){
      trocar(h, 1, i);
      h->tam= h->tam-1;
      maxHeapify(h, 1);
   
   }
   h->tam = tam;
   
}

void exibirMax(heap *h){
   int i;
   
   for(i = 1; i <= h->tam; i++){
      printf("%d ", h->a[i]);
   }
   printf("\n");
}
void lerArquivo(heap *h, FILE *arquivo){
	int i;
	
	while(fscanf(arquivo, "%d", &i) != EOF){
		adicionar(h, i);
	}
}



void minHeapify(heap *h, int i){
   int l, r, menor;
   l = esquerdo(i);
   r = direito(i);
   
   if(l <= h->tam && h->a[l] > h->a[i]){
      menor = l;
   }else{
      menor = i;
   }
   if(r <= h->tam && h->a[r] > h->a[menor]){
      menor = r;
   }
   if(menor != i){
      trocar(h, i, menor);
      minHeapify(h, menor);
   }
}

void CriarMinHeap(heap *h){
   int i;
   
   h->tam = h->comp;
   
   for(i = (h->comp/2); i>0; i--){
      minHeapify(h, i);
   }
   
}

void minHeapSort(heap *h){
   int i, tam;
   tam = h->tam;
   
   CriarMinHeap(h);
   
   for(i = h->comp; i>1; i--){
      trocar(h, 1, i);
      h->tam= h->tam-1;
      minHeapify(h, 1);
   
   }
   h->tam = tam;
   
}

void exibirMin(heap *h){
   int i;
   
   for(i = (h->comp - h->tam)+1; i <= h->comp; i++){
      printf("%d ", h->a[i]);
   }
   printf("\n");
}

void salvarArquivoMin(heap *h, FILE *arquivo){
	int i;
	
	for(i = (h->comp - h->tam) +1; i<=h->comp; i++){
		fprintf( arquivo, "%d ", h->a[i]);
	} 

}
int pai(int i){
   return (i/2);
 
}

int esquerdo(int i){
   return (i*2);
   
}

int direito(int i){
   return ((i*2)+1);
   
}


void salvarArquivoMax(heap *h, FILE *arquivo){
	int i;
	
	for(i = 1; i<=h->tam; i++){
		fprintf(arquivo, "%d ", h->a[i]);
	}

}
void help(char *name){
	printf("-h                     : Help\n");
	printf("-o <arquivo>           : Arquivo de Saida\n");
	printf("-f <arquivo>           : Arquivo de Entrada\n");
	printf("-m                     : Heap minimo\n");
	exit(-1);
}

void exception(char *name){
	printf("Erro \n");
	exit(-1);
}









