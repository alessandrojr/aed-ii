#ifndef _HEAP_H
#define _HEAP_H

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct heap{
   int *a;
   int tam;
   int comp;   

}heap;
void maxHeapSort(heap *h);
void minHeapify(heap *h, int i);
int pai(int i);
int esquerdo(int i);
int direito(int i);
void CriarMinHeap(heap *h);
void minHeapSort(heap *h);
void exibirMax(heap *h);
void exibirMin(heap *h);
void salvarArquivoMin(heap *h, FILE *arquivo);
void salvarArquivoMax(heap *h, FILE *arquivo);
void lerArquivo(heap *h, FILE *arquivo);
int adicionar(heap *h, int valor);
void init(heap *h, int t);
void trocar(heap *h, int i, int  maior);
void help(char *name);
void exception(char *name);
void maxHeapify(heap *h, int i);
void CriarMaxHeap(heap *h);



#endif

