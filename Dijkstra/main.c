#include "dijkstra.h"


int main(int argv, char *argc[]){
	Grafo *grafoI = montarGrafo(true, true);
    Grafo *grafoF = montarGrafo(true, true);
    int i;
    int flag = 0;
    int flagPrintEspecifico = 0;
    int verticeInicial = 0;
    int verticeFinal = 0;
    int flagSalvar = 0;
    char arquivoSaida[100];

    for(i = 1; i < argv; i++){
        if(strcmp(argc[i], "-h") == 0){
            printf("\n-h                  \n");
            printf("\n-f arquivo.dat - entrada\n");
            printf("\n-o arquivo.dat -  saida\n" );
            printf("\n-i inteiro     -  v�rtice inicial\n");
            printf("\n-l inteiro     -  v�rtice final\n");
        }
        if(strcmp(argc[i], "-f")== 0){
            entradaFinal(grafoI, argc[i+1]);
            flag = 1;
        } 
        if(strcmp(argc[i], "-i")== 0){
            djikstra(grafoI, grafoF, atoi(argc[i+1]));
            verticeInicial = atoi(argc[i+1]);

            flag = 1;
        } 
        if(strcmp(argc[i], "-o") == 0){
            strcpy(arquivoSaida, argc[i+1]);
        	flagSalvar = 1;
            flag = 1;
        }
        if(strcmp(argc[i], "-l")== 0){       
            flagPrintEspecifico = 1;
            verticeFinal = atoi(argc[i+1]);
        } 
    }
    if (flagPrintEspecifico == 1){
        PrintarDeterminado(grafoF, verticeInicial, verticeFinal);
    }
    else{
        Printar(grafoF, verticeInicial);
    }
    if(flagSalvar == 1){
        FILE *arquivo = fopen(arquivoSaida, "w");
        if (arquivo == NULL){
            printf("ERRO!!! - Arquivo invalido\n");
        }
        else{
            saidaTXT(grafoF, verticeInicial, arquivo);
        }
        fclose(arquivo); 
    }
    return 0;
}
    
