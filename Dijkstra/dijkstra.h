#ifndef __Djikstra__
#define __Djikstra__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VERTMAX 50
#define ARESTAMAX 20
#define true 1
#define false 0

typedef struct lista{
    int vertice1;
    int vertice2;
    int peso;
    struct lista* proximo;
} Lista;

typedef struct __GRAFO__
{
    int ponderado;
    int digrafo;
    int matriz[VERTMAX][VERTMAX];
    Lista *arestas;
}Grafo;

typedef struct listaauxiliar
{
    int vertice;
    int custo;
    int prev;
}Listaauxiliar;

Lista* criarLista();
void bubbleSort(Lista* l);
Lista* inserirLista(Lista* l, int vertice1, int vertice2, int peso);
Grafo *montarGrafo(int ponderado, int digrafo);
void adicionarGrafo(Grafo *GR, int vertice1, int vertice2, int peso);
void djikstra(Grafo *GR, Grafo *grafoFinal, int v_inicial);
void Printar(Grafo *GR, int verticeInicial);
void PrintarDeterminado(Grafo *GR, int inicial, int final);
void entradaFinal(Grafo *GR, char arquivo[]);
void saidaFinal(Grafo *GR, int verticeInicial, FILE *arquivo);

#endif