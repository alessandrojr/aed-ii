#include "dijkstra.h"
#include "Heap.c"

Lista* criarLista(){
	return NULL;
}
void djikstra(Grafo *GR, Grafo *grafoFinal, int v_inicial){
    bubbleSort(GR->arestas);
    int escolha;
    Heap *prioridade = (Heap*)malloc(sizeof(Heap));
    Lista *auxiliar;
    Listaauxiliar *geratriz[VERTMAX];
    Listaauxiliar *min;
    for(int i = 0; i < VERTMAX; i++){ 
        geratriz[i] = (Listaauxiliar*)malloc(sizeof(geratriz));
        geratriz[i]->vertice = i;
        geratriz[i]->custo = 99999;
        geratriz[i]->prev = -1;
        if(i == v_inicial){
            geratriz[i]->custo = 0;}
        min_heap_insert(prioridade, geratriz[i]);}
    construir_min_heap(prioridade);
    while(heap_vazia(prioridade) != 1){  
        min = heap_extract_min(prioridade);
        for(auxiliar = GR->arestas->proximo; auxiliar != NULL; auxiliar = auxiliar->proximo){
            if(auxiliar->vertice1 == min->vertice || auxiliar->vertice2 == min->vertice){   
                if(auxiliar->vertice1 == min->vertice){
                    escolha = auxiliar->vertice2;
                }
                else if(auxiliar->vertice2 == min->vertice){   
                    escolha = auxiliar->vertice1;
		}
                if(geratriz[escolha]->custo > min->custo + auxiliar->peso){
                    geratriz[escolha]->custo = min->custo + auxiliar->peso;
                    geratriz[escolha]->prev = min->vertice;
                    construir_min_heap(prioridade);
		}
	    }
	}
    
        if(min->prev != -1){
            adicionarGrafo(grafoFinal, v_inicial, min->vertice, min->custo);
	}
    }
}


void bubbleSort(Lista* l){
	Lista* auxiliar;
	int trocou = 0;	
	int tempV1, tempV2, tempP;
	while(trocou == 0){
		trocou = 1;
		for(auxiliar = l; auxiliar->proximo!=NULL; auxiliar = auxiliar->proximo){
			if(auxiliar->peso > auxiliar->proximo->peso){
				tempP = auxiliar->peso;
				auxiliar->peso = auxiliar->proximo->peso;
				auxiliar->proximo->peso = tempP;
				
				tempV1 = auxiliar->vertice1;
				auxiliar->vertice1 = auxiliar->proximo->vertice1;
				auxiliar->proximo->vertice1 = tempV1;
				
				tempV2 = auxiliar->vertice2;
				auxiliar->vertice2 = auxiliar->proximo->vertice2;
				auxiliar->proximo->vertice2 = tempV2;
				trocou = 0;
			}
		}
	}
}


Grafo *montarGrafo(int ponderado, int digrafo)
{
    Grafo *GR = (Grafo*)malloc(sizeof(Grafo));
    Lista *arestas = (Lista*)malloc(sizeof(Lista));
    GR->arestas = arestas;
    int i, j;
    for(i = 0; i < VERTMAX; i++){
        for(j = 0; j < VERTMAX; j++){
            GR->matriz[i][j] = 0;
        }
    }
    GR->digrafo = digrafo;
    GR->ponderado = ponderado;

    
    return GR;
}


Lista* inserirLista(Lista* l, int vertice1, int vertice2, int peso){
	Lista* novo = (Lista*)malloc(sizeof(Lista));
	novo->vertice1 = vertice1;
	novo->vertice2 = vertice2;
	novo->peso = peso;
	if(l==NULL){
		novo->proximo = NULL;
		return novo;
	}
	novo->proximo = l;
	return novo;
}

void adicionarGrafo(Grafo *GR, int vertice1, int vertice2, int peso)
{
    GR->arestas = inserirLista(GR->arestas, vertice1, vertice2, peso);

    if(GR->ponderado == true && GR->digrafo == true)
    {
        GR->matriz[vertice1][vertice2] = peso;
    }

    else if(GR->ponderado == true && GR->digrafo == false)
    {
        GR->matriz[vertice1][vertice2] = peso;
        GR->matriz[vertice2][vertice1] = peso;
    }

    else if(GR->ponderado == false && GR->digrafo == true)
    {
        GR->matriz[vertice1][vertice2] = 1;
    }

    else if(GR->ponderado == false && GR->digrafo == false)
    {
        GR->matriz[vertice1][vertice2] = 1;
        GR->matriz[vertice2][vertice1] = 1;
    }
}

void PrintarDeterminado(Grafo *GR, int inicial, int final)
{
    if(GR->matriz[inicial][final] != 0 && GR->ponderado == false)
    {
        printf("%d", GR->matriz[inicial][final]);
    }
    else if(GR->matriz[inicial][final] != 0 && GR->ponderado == true)
    {
        printf("%d", GR->matriz[inicial][final]);
    }
    printf("\n");
}


void Printar(Grafo *GR, int verticeInicial)
{
    int i, j;
    printf("%d:%d ", verticeInicial, 0);
    for(i = 0; i < VERTMAX; i++)
    {
        for(j = 0; j < VERTMAX; j++)
        {
            if(GR->matriz[i][j] != 0 && GR->ponderado == false)
            {
                printf("%d:%d ",j, GR->matriz[i][j]);
            }
            else if(GR->matriz[i][j] != 0 && GR->ponderado == true)
            {
                printf("%d:%d ",j,GR->matriz[i][j]);
            }
        }
    }
    printf("\n");
}



void entradaFinal(Grafo *GR, char arquivo[]){
    FILE *abrirArquivo = fopen(arquivo, "r");
    if(abrirArquivo == NULL){
        printf("erro abrir arquivo entrada\n");
        exit(0);
    }
    else{
        int vertices, arestas;
        fscanf(abrirArquivo, "%d %d\n", &vertices, &arestas);
        char entrada[10];
        int i;
        for(i = 0; i < arestas; i++){
            int origem, destino, peso;
            fscanf(abrirArquivo, "%d %d %d\n", &origem, &destino, &peso);
            adicionarGrafo(GR, origem, destino, peso);
       
        }

    }
    fclose(abrirArquivo);            
}



void saidaFinal(Grafo *GR, int verticeInicial, FILE *arquivo){
    int i, j;
    fprintf(arquivo, "%d:%d ", verticeInicial, 0);
    for(i = 0; i < VERTMAX; i++)
    {
        for(j = 0; j < VERTMAX; j++)
        {
            if(GR->matriz[i][j] != 0 && GR->ponderado == true)
            {
                fprintf(arquivo, "%d:%d ",j, GR->matriz[i][j]);
            }
            else if(GR->matriz[i][j] != 0 && GR->ponderado == true)
            {
                fprintf(arquivo, "%d:%d ",j,GR->matriz[i][j]);
            }
        }
    }
    fprintf(arquivo, "\n");
}


