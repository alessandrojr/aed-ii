#ifndef _PRIM_H
#define _PRIM_H

#include <string.h>
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#define maximo 999

typedef struct{
	int vertO;
	int vertD;
    int peso;
}Aresta;

struct no{
	Aresta* Aresta;
	struct no *proximo;	
};

typedef struct Grafo{	
	int quantidade_de_Arestas;
	int quantidade_de_Vertices;
	int countA;
	struct no* VetArestas[maximo];
	
}Grafo;

typedef struct{
    int contador;
    Aresta* Vet[maximo];
}VetorAresta;


typedef struct{
    int contador;
    int Vet[maximo];
}VetorVertice;

void removerAresta(VetorAresta *VetAresta, int vertO, int vertD);
void montar_VetorArestas(Grafo *Grafo, VetorAresta *VetAresta);
void montar_VetorVertices(Grafo *Grafo, VetorVertice *VetVertice);
int verificacaoDeVertice1(VetorVertice *VetVertice, int vertO, int vertD);
int verificacaoDeVertice2(VetorVertice *VetVertice, int vertO, int vertD);
void prim(Grafo *GrafoI, Grafo *GrafoF, VetorAresta *VetArestaF);
void printarVetorVertice(VetorVertice *Vet);
void printarVetorArestas(VetorAresta *VetAresta);
void printarGrafo(Grafo *Grafo);
int calcularPesoTotal(VetorAresta *VetAresta);
void entradaFinal(Grafo *Grafo, char arquivo[]);
void saidaFinal(VetorAresta *VetAresta, FILE *arquivo);
void comecarVetorArestas(VetorAresta *VetAresta, int valor);
void comecarVetorVertices(VetorVertice *Vet);
Aresta* comecarAresta(int vertO, int vertD, int peso1);
struct no* comecarNo(int vertO, int vertD, int peso);
void montar_Grafo(Grafo *Grafo, int quantidade_de_Arestas, int quantidade_de_Vertices);
void selectionSort(VetorAresta *VetAresta);
void selectionSortArestas(VetorAresta *VetAresta);
void acrescentarArestaVetor (VetorAresta *VetAresta, Aresta *Aresta);
void acrescentarVertice(VetorVertice *VetVertice, int numero);
int acrescentarVerticeCondicao(VetorVertice *VetVerticeI, VetorVertice *VetVerticeF, int vertO, int vertD);
int acrescentarAresta(Grafo *Grafo, int vertO, int vertD, int peso);
void removerVertice(VetorVertice *VetVertice, int indice);

#endif