#include "prim.h" 


void comecarVetorArestas(VetorAresta *VetAresta, int valor){
    VetAresta->contador = valor;

}

void comecarVetorVertices(VetorVertice *Vet){
	Vet->contador = 0;
}
void entradaFinal(Grafo *Grafo, char arquivo[]){
    FILE *arquivoLeitura = fopen(arquivo, "r");
    if(arquivoLeitura == NULL){
        printf("erro na entrada\n");
        exit(0);
    }
    else{
    	int numeroVertice, numeroAresta;
        fscanf(arquivoLeitura, "%d %d\n", &numeroVertice, &numeroAresta);
       	montar_Grafo(Grafo, numeroAresta, numeroVertice);
        int i;
        for(i = 0; i < numeroAresta; i++){
        	int origem, destino, peso;
       		fscanf(arquivoLeitura, "%d %d %d\n", &origem, &destino, &peso);
            acrescentarAresta(Grafo, origem, destino, peso);
       
        }}}

Aresta* comecarAresta(int vertO, int vertD, int peso1){
    Aresta *Aresta1;
    Aresta1 = (Aresta *)malloc(sizeof(Aresta));
    Aresta1->vertO = vertO;
    Aresta1->vertD = vertD;
    Aresta1->peso = peso1;
    return Aresta1;
}

struct no* comecarNo(int vertO, int vertD, int peso){
	Aresta *Aresta = comecarAresta(vertO, vertD, peso);
	struct no *novoNo = (struct no *)malloc(sizeof(struct no));
	novoNo->proximo = NULL;
	novoNo->Aresta = Aresta;
	return novoNo;
}



void selectionSort(VetorAresta *VetAresta){
    int i, j, minimo;
    Aresta *temporario;
    for(i = 0; i < VetAresta->contador - 1; i++){
        minimo = i;
        for(j = i+1; j < VetAresta->contador; j++){
            if(VetAresta -> Vet[j] -> peso < VetAresta -> Vet[minimo] -> peso){
                minimo = j;
            }
        }
        if(i != minimo){
            	temporario = VetAresta -> Vet[i];
            	VetAresta -> Vet[i] = VetAresta -> Vet[minimo];
            	VetAresta -> Vet[minimo] = temporario;
        }
    }
}

void selectionSortArestas(VetorAresta *VetAresta){
    	int i, j, minimo;
    	Aresta *temporario;
    	for(i = 0; i < VetAresta->contador - 1; i++){
        	minimo = i;
       	 for(j = i+1; j < VetAresta->contador; j++){
           	 if(VetAresta -> Vet[j] -> vertO < VetAresta -> Vet[minimo] -> vertO){
                	minimo = j;
            }
        }
        	if(i != minimo){
            	temporario = VetAresta -> Vet[i];
            	VetAresta -> Vet[i] = VetAresta -> Vet[minimo];
            	VetAresta -> Vet[minimo] = temporario;
        }
    }
}


void acrescentarArestaVetor (VetorAresta *VetAresta, Aresta *Aresta){
		VetAresta->Vet[VetAresta->contador] = Aresta;
		VetAresta->contador++;
}

void acrescentarVertice(VetorVertice *VetVertice, int numero){
		VetVertice->Vet[VetVertice->contador] = numero;
		VetVertice->contador++;
}

int acrescentarVerticeCondicao(VetorVertice *VetVerticeI, VetorVertice *VetVerticeF, int vertO, int vertD){
	int i, j;
	int origem = 0;
	int destino = 0;
	for(i = 0; i < VetVerticeI->contador; i++){
		if(VetVerticeI->Vet[i] == vertO){
			origem = -1;
		}
	}

	for(i = 0; i < VetVerticeI->contador; i++){
		if(VetVerticeI->Vet[i] == vertD){
			destino = -1;
		}
	}
	if(origem == -1 && destino == 0){
		VetVerticeF->Vet[VetVerticeF->contador] = vertD;
		VetVerticeF->contador++;
	}
	if (destino == -1 && origem == 0){
		VetVerticeF->Vet[VetVerticeF->contador] = vertO;
		VetVerticeF->contador++;
	}
}
void prim(Grafo *GrafoI, Grafo *GrafoF, VetorAresta *VetArestaF){
	
	VetorAresta VetAresta;
	
	VetorVertice VetVerticeI, VetVerticeF;
	
	comecarVetorArestas(&VetAresta, 0);
	
	comecarVetorVertices(&VetVerticeI);
	
	comecarVetorVertices(&VetVerticeF);
	
	montar_VetorArestas(GrafoI, &VetAresta);
	
	montar_VetorVertices(GrafoI, &VetVerticeI);
	
	int j;
	
	int variavel = 0;
	
	for(j = 0; j <= VetAresta.contador; j++){
		if(GrafoF->countA == 0){
			acrescentarAresta(GrafoF, VetAresta.Vet[0]->vertO, VetAresta.Vet[0]->vertD, VetAresta.Vet[0]->peso);
			acrescentarArestaVetor(VetArestaF, VetAresta.Vet[0]);
			removerVertice(&VetVerticeI, VetAresta.Vet[0]->vertD);
			removerVertice(&VetVerticeI, VetAresta.Vet[0]->vertO);
			acrescentarVertice(&VetVerticeF, VetAresta.Vet[0]->vertO);
			acrescentarVertice(&VetVerticeF, VetAresta.Vet[0]->vertD);
			removerAresta(&VetAresta, VetAresta.Vet[0]->vertO, VetAresta.Vet[0]->vertD);
		}
		
		else if(GrafoF->countA == GrafoI->quantidade_de_Vertices - 1){
			break;
		}
		
		else{
			int i;
			
				for(i = 0; i < VetAresta.contador; i++){
					int ambosVerticesNoVetor = verificacaoDeVertice1(&VetVerticeF, VetAresta.Vet[variavel]->vertO, VetAresta.Vet[variavel]->vertD);
					int ambosVerticesForaVetor = verificacaoDeVertice2(&VetVerticeF, VetAresta.Vet[variavel]->vertO, VetAresta.Vet[variavel]->vertD);
					if(VetVerticeI.contador == 0){
						break;
				}
						else{
							if(ambosVerticesForaVetor == -1){
						 	variavel++;
					}
						
						
						else if(ambosVerticesNoVetor == -1){
							removerAresta(&VetAresta, VetAresta.Vet[variavel]->vertO, VetAresta.Vet[variavel]->vertD);
							variavel = 0;	
					
					}
				else{
					acrescentarAresta(GrafoF, VetAresta.Vet[variavel]->vertO, VetAresta.Vet[variavel]->vertD, VetAresta.Vet[variavel]->peso);	
					acrescentarArestaVetor(VetArestaF, VetAresta.Vet[variavel]);
					removerVertice(&VetVerticeI, VetAresta.Vet[variavel]->vertO);
					removerVertice(&VetVerticeI, VetAresta.Vet[variavel]->vertD);
					acrescentarVerticeCondicao(&VetVerticeF, &VetVerticeF, VetAresta.Vet[variavel]->vertO, VetAresta.Vet[variavel]->vertD);
					removerAresta(&VetAresta, VetAresta.Vet[variavel]->vertO, VetAresta.Vet[variavel]->vertD);
					variavel = 0;	
					}
				}
			}
		}
	}

}
int acrescentarAresta(Grafo *Grafo, int vertO, int vertD, int peso){
	struct no *no1;
    no1 = comecarNo(vertO, vertD, peso);
    struct no *temporario = Grafo->VetArestas[vertO];
    no1->proximo = temporario;
    temporario = no1;
    Grafo->VetArestas[vertO] = no1;
    Grafo->countA++;
}

void removerVertice(VetorVertice *VetVertice, int indice){
		int i, j;
		for(i = 0; i < VetVertice->contador; i++){
			if(VetVertice->Vet[i] == indice){
				for(j = i; j < VetVertice->contador - 1; j++){
					VetVertice->Vet[j] = VetVertice->Vet[j + 1];
				}
				VetVertice->contador--;
				break;
			}
		}
	}

void removerAresta(VetorAresta *VetAresta, int vertO, int vertD){
    int i, j;
    for(i = 0; i < VetAresta -> contador; i++){
        if(VetAresta -> Vet[i] -> vertO == vertO && VetAresta -> Vet[i] -> vertD == vertD){
            for(j = i; j < VetAresta -> contador - 1; j++){
                VetAresta -> Vet[j] = VetAresta -> Vet[j+1];
            }
        VetAresta -> contador--;
        break;
        }
    }
}



void montar_VetorArestas(Grafo *Grafo, VetorAresta *VetAresta){
	int i;
	for(i = 0; i <= Grafo->quantidade_de_Vertices; i++){
		struct no *temporario = Grafo->VetArestas[i];
		while(temporario != NULL){
			VetAresta->Vet[VetAresta->contador] = temporario->Aresta;
			VetAresta->contador++;
			temporario = temporario->proximo;
		}
	}
	selectionSort(VetAresta);
}

void montar_VetorVertices(Grafo *Grafo, VetorVertice *VetVertice){
	int i;
	for(i = 0; i <= Grafo->quantidade_de_Vertices; i++){
		VetVertice->Vet[VetVertice->contador] = i;
		VetVertice->contador++;
	}
}




int verificacaoDeVertice1(VetorVertice *VetVertice, int vertO, int vertD){
	int i, j;
	int resultado = 0;
	for(i = 0; i < VetVertice->contador; i++){
		if(VetVertice->Vet[i] == vertO){
			for(j = 0; j < VetVertice->contador; j++){
				if(VetVertice->Vet[j] == vertD){
					resultado = -1;
				}
			}
		}
	}
	return resultado;
}


int verificacaoDeVertice2(VetorVertice *VetVertice, int vertO, int vertD){
	int i, j;
	int flag1 = 0;
	int flag2 = 0;
	int resultado = 0;
	for(i = 0; i < VetVertice->contador; i++){
		if(VetVertice->Vet[i] == vertO){
			flag1 = -1;
		}
	}

	for(i = 0; i < VetVertice->contador; i++){
		if(VetVertice->Vet[i] == vertD){
			flag2 = -1;
		}
	}
	if(flag1 == 0 && flag2 == 0){
		resultado = -1;
	}
	return resultado;

}




void printarVetorVertice(VetorVertice *Vet){
	int i;
	for(i = 0; i<Vet->contador; i++){
		printf("%d - ", Vet->Vet[i]);
	}
	printf("\n");
}

void printarVetorArestas(VetorAresta *VetAresta){
    int i;
    for(i = 0; i < VetAresta->contador; i++ ){
        printf("(%d, %d)", VetAresta->Vet[i]->vertO, VetAresta->Vet[i]->vertD);
    }
    printf("\n");
}

void printarGrafo(Grafo *Grafo){
    int i;
    for(i = 0; i <= Grafo->quantidade_de_Vertices; i++){
        printf("%d: ", i);
        struct no *temporario = Grafo->VetArestas[i];
        while(temporario != NULL){
            printf("(%d, %d)", temporario->Aresta->vertO, temporario->Aresta->vertD);
            temporario = temporario->proximo;
        }
        printf("\n");
    }
}



int calcularPesoTotal(VetorAresta *VetAresta){
    int pesoTotal = 0;
    int i;
    for(i = 0; i < VetAresta->contador; i++ ){
        pesoTotal = pesoTotal + VetAresta->Vet[i]->peso;
    }
    return pesoTotal;
}

void montar_Grafo(Grafo *Grafo, int quantidade_de_Arestas, int quantidade_de_Vertices){
	Grafo->quantidade_de_Vertices = quantidade_de_Vertices;
	Grafo->quantidade_de_Arestas = quantidade_de_Arestas;
	Grafo->countA = 0;
	int i;
	for(i = 0; i <= quantidade_de_Vertices; i++){
		Grafo->VetArestas[i] = NULL;
	}
}


void saidaFinal(VetorAresta *VetAresta, FILE *arquivo){
	selectionSortArestas(VetAresta);
	int i;
	for (i = 0; i < VetAresta->contador; ++i)
	{
		fprintf(arquivo, "(%d, %d)", VetAresta->Vet[i]->vertO, VetAresta->Vet[i]->vertD);
	}
}


















