#include "prim.h"

int main(int argv, char *argc[]){
    Grafo GrafoInicial;
    Grafo GrafoFinal;
    VetorAresta VetAresta;    
    int i;
    int flag = 0;
    for(i = 1; i < argv; i++){
        if(strcmp(argc[i], "-h") == 0){
            printf("\n-h             -  ajuda\n");
            printf("\n-f arquivo.dat -  entrada\n");
            printf("\n-o arquivo.dat -  saida\n" );
            printf("\n-s inteiro     -  solu��o em ordem crescente\n");
        }
        if(strcmp(argc[i], "-f")== 0){
            entradaFinal(&GrafoInicial, argc[i+1]);
            montar_Grafo(&GrafoFinal, GrafoInicial.quantidade_de_Arestas, GrafoInicial.quantidade_de_Vertices - 1);
            prim(&GrafoInicial, &GrafoFinal, &VetAresta);
        	flag = 1;
        }
        if(strcmp(argc[i], "-o") == 0){
            FILE *arquivo = fopen(argc[i+1], "w");
            if (arquivo == NULL){
                printf("ERRO!!!!! - arquivo invalido\n");
            }
            else{
                saidaFinal(&VetAresta, arquivo);
            }
            flag = 1; 
        }
        if(strcmp(argc[i], "-s")== 0){
            printarVetorArestas(&VetAresta);        
            flag = 0;
        }
    }
    if(flag == 1){
    	int peso = 0;
        int j;
        for(j = 0;j < VetAresta.contador; j++ ){
            peso += VetAresta.Vet[j]->peso;
        }
        printf("%d\n", peso);
    }
    return 0;
}

