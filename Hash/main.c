#include "hash.h"

int main(int argc, char * argv[]){

	hash Ha_sh;
	
	FILE *arquivo;
	char opcao;
	int tamanho;
	tamanho = 11;
	if(argc > 7){
   	exception(argv[0]);
  	}		
	
	while((opcao = getopt(argc, argv, "ho:f:mMa:s:")) > 0){
		switch(opcao){
			case 'h':
				help(argv[0]);
				break;
				
			case 'o': 
			
				arquivo = fopen(optarg, "wb+");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				salvarArquivo(&Ha_sh, arquivo);
				fclose(arquivo);
				break;
				
			case 'f': 
				
				inicializar(&Ha_sh, tamanho);
				arquivo = fopen(optarg, "rb");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				lerArquivo(&Ha_sh, arquivo);
				fclose(arquivo);
				Printa_Hash(&Ha_sh);
  				printf("\n");
				break;
			case 'm': 
				
				tamanho = atoi(argv[2]);
				break;
				
		}
	}
	return 0;
	

}

