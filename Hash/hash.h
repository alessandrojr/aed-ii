#ifndef _HASH_H
#define _HASH_H

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>


typedef struct node{
   int dado;
   struct node *proximo;
}node;

typedef struct hash{
   int tamanho;
   node* *lista;

}hash;

void salvarArquivo(hash *Ha_sh, FILE *arquivo);
void lerArquivo(hash *Ha_sh, FILE *arquivo);
void help(char *nome);
void exception(char *nome);
void inicializar(hash *Ha_sh, int tamanho);
int H(hash *Ha_sh, int chave);
node* Montar_No(int valor);
void hash_adicionar(hash *Ha_sh, int valor);
node* hash_Busca(hash *Ha_sh, int valor);
void Printa_Hash(hash *Ha_sh);

#endif

