#include "hash.h"



void inicializar(hash *Ha_sh, int tamanho){
   Ha_sh->lista = (node* *)malloc(sizeof(node*)*(tamanho));
   Ha_sh->tamanho = tamanho;

}

node* hash_Busca(hash *Ha_sh, int valor){
   node *temp;
   
   temp = Ha_sh->lista[H(Ha_sh, valor)];
   while(temp != NULL){
      if(temp->dado == valor){
         return temp;   
      }
      temp = temp->proximo;
   }
   return temp;
  
}


int H(hash *Ha_sh, int chave){
   return (chave % Ha_sh->tamanho);
}  

node* Montar_No(int valor){
   node *delta = (node *)malloc(sizeof(node));
   delta->proximo = NULL;
   delta->dado = valor;
}



void hash_adicionar(hash *Ha_sh, int valor){
   node *delta, *temp, *y;
   y = NULL;
   delta = Montar_No(valor);
   temp = Ha_sh->lista[H(Ha_sh, valor)];
   
   while(temp != NULL){
      y = temp;
      temp = temp->proximo;
   }
   
   if(y == NULL){
      Ha_sh->lista[H(Ha_sh, valor)] = delta;
   }else{
      y->proximo = delta;
   }
      
}
void salvarArquivo(hash *Ha_sh, FILE *arquivo){
	
   int i;
   node *temp;
   
   for(i = 0; i < Ha_sh->tamanho; i++){
      temp = Ha_sh->lista[i];
      fprintf(arquivo, "%d: ", i);
      while(temp != NULL){
         fprintf(arquivo, "%d ", temp->dado);
         temp = temp->proximo;
      }
      fprintf(arquivo, "\n");
   }


}

void lerArquivo(hash *Ha_sh, FILE *arquivo){
	int i;
	
	while(fscanf(arquivo, "%d", &i) != EOF){
	
		hash_adicionar(Ha_sh, i);
	}

}

void help(char *nome){
	printf("-h                     : Ajuda\n");
	printf("-o <arquivo>           : Arquivo de saida\n");
	printf("-f <arquivo>           : Arquivo de entrada\n");
	exit(-1);
}

void exception(char *nome){
	printf("ERRO: parâmetros conflitantes \n");
	exit(-1);
}


void Printa_Hash(hash *Ha_sh){
   int i;
   node *temp;
   
   for(i = 0; i < Ha_sh->tamanho; i++){
      temp = Ha_sh->lista[i];
      printf("%d: ", i);
      while(temp != NULL){
         printf("%d ", temp->dado);
         temp = temp->proximo;
      }
      
      printf("\n");
   }
}






