#include "avl.h"

int main(int argc, char **argv){

	arvore tree;
	init(&tree);
	FILE *arquivo;
	char opcao;
	node *delta, *beta;
	
	if(argc > 5){
   	exception(argv[0]);
   	}
	
	while((opcao = getopt(argc, argv, "ho:f:mMa:s:")) > 0){
		switch(opcao){
			case 'h': 
				help(argv[0]);
				break;
				
			case 'o': 
				arquivo = fopen(optarg, "wb+");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				salvarArquivo(tree.raiz, arquivo);
				fclose(arquivo);
				break;
				
			case 'f': 
				arquivo = fopen(optarg, "rb");
				if(arquivo == NULL){
					exception(argv[0]);
				}
				lerArquivo(&tree, arquivo);
				fclose(arquivo);
				percursoPreOrdem(tree.raiz);
  				printf("\n");
				break;
				
			case 'M':
				delta = Maximo_arvore(tree.raiz);
				printf("%d\n", delta->valor);
				break;
			case 'm': 
				delta = treeMinimum(tree.raiz);
				printf("%d\n", delta->valor);
				break;
				
			case 'a': 
				delta = Busca_arvore(&tree, atoi(optarg)); 
				if(delta == NULL){
					
					printf("-1\n");
					break;
				}
				beta = Antecessor_arvore(delta);
				
				if(beta != NULL){
					printf("%d\n", beta->valor);
				}else{
					printf("-1\n");
					break;
				}
				break;
			case 's':
				delta = Busca_arvore(&tree, atoi(optarg));
				if(delta == NULL){
					printf("-1\n");
					break;
				}
				beta = Sucessor_arvore(delta);
				
				if(beta != NULL){
					printf("%d\n", beta->valor);
				}else{
					printf("-1\n");
					break;
				}
				break;
			
			default:
				return 1;			
		}	
	}
	
	return 0;

}

