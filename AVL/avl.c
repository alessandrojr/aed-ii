#include "avl.h"


void init(arvore *inicio){
	inicio->raiz = NULL;

}


void balancear(arvore *tree, node *delta){
   node *u;      
   while(delta != NULL ){		
		if (calcular_altura(delta->esq) - calcular_altura(delta->dir) >= 2){
			u = delta->esq;
			if(calcular_altura(u->esq) > calcular_altura (u->dir)){
				rotacao_direito(delta);}
			else{
				dupla_rotacao_direita(delta);}}
		if(calcular_altura(delta->dir) - calcular_altura(delta->esq) >= 2){				
			u = delta->dir;
			if(calcular_altura(u->dir) > calcular_altura (u->esq)){
				rotacao_esq(delta);}
			else{
				dupla_rotacao_esquerda(delta);}}
		if(delta->pai == NULL){
			tree->raiz = delta;}
		delta=delta->pai;}}
		
node* Busca_arvore(arvore *tree, int valor){
	node *x;
	x = tree->raiz;
	
	while(x != NULL && x->valor != valor){
		if(x->valor > valor){
			x = x->esq;
		}else{
			x = x->dir;
		}
	}
	
	if(x == NULL){
		return NULL;
	}else{
		return x;
	}
	
	
}



node* treeMinimum(node *x){
	while(x->esq != NULL && x != NULL){
		x = x->esq;
	}
	
	return x;
}



node* Sucessor_arvore(node *x){
	node *beta;
	if(x->dir != NULL){
		return treeMinimum(x->dir);
	}
	
	beta = x->pai;
	while(beta != NULL && x == beta->dir){
		x = beta;
		beta = beta->pai;
	}	
	return beta;

}

void percursoEmOrdem(node *x){
	if(x!=NULL){
		percursoEmOrdem(x->esq);
		printf("%d\n", x->valor);
		percursoEmOrdem(x->dir);	
	}
}


void percursoPreOrdem(node *a){
	printf("(%d ", a->valor);
	if(a->esq == NULL){
		if(a->dir == NULL){
			printf("(x)");
		}else{
			printf("(x ");
		}
			
	}else{
		percursoPreOrdem(a->esq);
	}
	
	if(a->dir == NULL){
		if(a->esq == NULL){
			printf(" (x)");
		}else{
			printf(" x)");
		}
	}else{
		percursoPreOrdem(a->dir);
	}
	printf(")");

}


void percursoPosOrdem(node *x){
	if(x!=NULL){
		percursoPosOrdem(x->esq);
		percursoPosOrdem(x->dir);
		printf("%d\n", x->valor);	
	}
}



node* criarnode(int valor){
	node *delta = (node *)malloc(sizeof(node));
	delta->valor = valor;
	delta->pai = NULL;
	delta->esq = NULL;
	delta->dir = NULL;
	
	return delta;
}



void treeInsert(arvore *tree, int valor){
	node *x, *beta, *delta;	
	x = tree->raiz;
	beta = NULL;
	delta = criarnode(valor);
	while(x != NULL){
		beta = x;
		if(delta->valor < x->valor){
			x = x->esq;
		}else {
			x = x->dir;
		}
	}
	delta->pai = beta;
	if(beta == NULL){
		tree->raiz = delta;	
	}else if(beta->valor > delta->valor){
		beta->esq = delta;
	}else{
		beta->dir = delta;
	}

	balancear(tree, delta);
}




node* Antecessor_arvore(node *x){
   node *beta;
   if(x->esq != NULL){
      return Maximo_arvore(x->esq);
      
   }
   
   beta = x->pai;
   while(beta != NULL && x == beta->esq){
      x = beta;
      beta = beta->pai;
   }
   return beta;

}



node* Maximo_arvore(node *x){
   while(x->dir != NULL && x != NULL){
      x = x->dir;
   }
   return x;

}


int max(int esq, int dir){

	if(esq > dir){
		return esq;

	}else{
		return dir;
	}
}


int calcular_altura(node *x){
	int h, altura_esq, altura_direito;
	
	if (x == NULL){
		return 0;
	}
	else{
        	altura_direito   = calcular_altura(x->dir);
        	altura_esq  = calcular_altura(x->esq);	
		h = max(altura_esq, altura_direito);
		
		return h+1;
	}
}



void rotacao_direito(node *x){
	node *beta = x->esq;
	node *y_direito = beta->dir;
	
	beta->pai = x->pai;
	if(x->pai != NULL){
	   if(x == x->pai->dir){
		   x->pai->dir = beta;
		}else{
		   x->pai->esq = beta;
		}
	}
	beta->dir = x;
	x->pai = beta;
	x->esq = y_direito;
	if(y_direito != NULL){
		y_direito->pai = x;
	}
	
}

void rotacao_esq(node *x){
	
	node *beta = x->dir;
	node *y_esq = beta->esq;
	
	beta->pai = x->pai;
	if(x->pai != NULL){
	   if(x == x->pai->dir){
		   x->pai->dir = beta;
		}else{
		   x->pai->esq = beta;
		}
	}
	beta->esq = x;
	x->pai = beta;
	x->dir = y_esq;
	if(y_esq != NULL){
		y_esq->pai = x;
	}
		
}


void dupla_rotacao_direita(node *x){
	rotacao_esq(x->esq);
	rotacao_direito(x);

}

void dupla_rotacao_esquerda(node *x){
	rotacao_direito(x->dir);
	rotacao_esq(x);
}



void salvarArquivo(node *a, FILE *arquivo){
	
	fprintf(arquivo, "(%d ", a->valor);
	if(a->esq == NULL){
		if(a->dir == NULL){
			fprintf(arquivo, "(x)");
		}else{
			fprintf(arquivo, "(x ");
		}
			
	}else{
		salvarArquivo(a->esq, arquivo);
	
	}
	
	if(a->dir == NULL){
		if(a->esq == NULL){
			fprintf(arquivo, " (x)");
		}else{
			fprintf(arquivo, " x)");
		}
	}else{
		salvarArquivo(a->dir, arquivo);
	}
	
	fprintf(arquivo, ")");

}

void lerArquivo(arvore *tree, FILE *arquivo){
	int i;
	
	while(fscanf(arquivo, "%d", &i) != EOF){
		treeInsert(tree, i);
	}

}

void help(char *name){
	printf("-h                     : Help\n");
	printf("-o <arquivo>           : Arquivo de Saída\n");
	printf("-f <arquivo>           : Arquivo de Entrada\n");
	printf("-M                     : Maior Elemento\n");
	printf("-m                     : Menor Elemento\n");
	printf("-a <elemento>          : Antecessor\n");
	printf("-s <elemento>          : Sucessor\n");
	exit(-1);
}

void exception(char *name){
	printf("Conflito de parametros \n");
	exit(-1);
}

