#ifndef _AVL_H
#define _AVL_H

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct node{
	struct node *pai;
	struct node *esq;
	struct node *dir;	
	int valor;

}node;

typedef struct arvore{
	node *raiz;

}arvore;

void init(arvore *inicio);
void percursoPreOrdem(node *x);
void percursoEmOrdem(node *x);
void percursoPosOrdem(node *x);
node* criarnode(int valor);
node* Sucessor_arvore(node *x);
node* Maximo_arvore(node *x);
node* Antecessor_arvore(node *x);
int calcular_altura(node *x);
int max(int esq, int dir);
void treeInsert(arvore *tree, int valor);
node* Busca_arvore(arvore *tree, int valor);
node* treeMinimum(node *x);
void rotacao_esq(node *x);
void dupla_rotacao_esquerda(node *x);
void dupla_rotacao_direita(node *x);
void salvarArquivo(node *a, FILE *arquivo);
void lerArquivo(arvore *tree, FILE *arquivo);
void balancear(arvore *tree, node *delta);
void rotacao_direito(node *x);
void help(char *name);
void exception(char *name);
#endif

