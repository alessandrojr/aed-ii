#include "rn.h"

void balancear(arvore *tree, node *delta){
   node *u, *tio;    
   while(delta != NULL ){
    if(delta->cor == 1 && delta->pai->cor == 1){
         tio = tio(delta);		
	   if(delta->pai->cor == 1 && tio->cor == 1){
		   delta->pai->cor = 0;
		   tio->cor = 0;
		   if(tree->raiz == delta->pai->pai){
		      delta->pai->pai->cor = 0;}
		   else{
		      delta->pai->pai->cor = 1;}}	   										
	   else if(delta->pai->cor == 1 && tio->cor == 0){				
        	
			if(delta->pai->esquerdo == delta && delta->pai->pai->esquerdo == delta->pai){
            	    rotacao_direito(delta->pai->pai);
            	    delta->pai->cor = 0;
                    delta->pai->direito->cor = 1;}
			else if(delta->pai->direito == delta && delta->pai->pai->direito == delta->pai){
               rotacao_esquerdo(delta->pai->pai);
               delta->pai->cor = 0;
               delta->pai->esquerdo->cor = 1;}
    		
			else if(delta->pai->direito == delta && delta->pai->pai->direito != delta->pai){
               rotacao_esquerdo(delta->pai);
            
               rotacao_direito(delta->pai);
               delta->cor = 0;
               delta->direito->cor = 1;
               if(delta->esquerdo != NULL){
                  delta->esquerdo->cor = 0;}}
               

               else if(delta->pai->esquerdo == delta && delta->pai->pai->esquerdo != delta->pai){
               rotacao_direito(delta->pai);
            
               rotacao_esquerdo(delta->pai);
               delta->cor = 0;
               delta->esquerdo->cor = 1;
               if(delta->direito != NULL){
                  delta->direito->cor = 0;}}}}
  
    if(delta->pai == NULL){
	tree->raiz = delta;
	tree->raiz->cor = 0;
    }
    delta=delta->pai;
  }
}

void init(arvore *inicio){
	inicio->raiz = NULL;

}

void percursoEmOrdem(node *alfa){
	if(alfa!=NULL){
		percursoEmOrdem(alfa->esquerdo);
		printf("%d\n", alfa->Valor);
		percursoEmOrdem(alfa->direito);	
	}
}

void percursoPreOrdem(node *a){	
	printf("(%d%s ", a->Valor, Cor(a));
	if(a->esquerdo == NULL){
		if(a->direito == NULL){
			printf("(alfa)");
		}else{
			printf("(alfa");
		}			
	}
	else{
		percursoPreOrdem(a->esquerdo);	
	}
	
	if(a->direito == NULL){
		if(a->esquerdo == NULL){
			printf(" (alfa)");
		}else{
			printf(" alfa)");
		}
	}
	else{
		percursoPreOrdem(a->direito);
	}	
	printf(")");
}


void percursoPosOrdem(node *alfa){
	if(alfa!=NULL){
		percursoPosOrdem(alfa->esquerdo);
		percursoPosOrdem(alfa->direito);
		printf("%d\n", alfa->Valor);	
	}
}

node* criarno(int valor){
	node *delta = (node *)malloc(sizeof(node));
	delta->Valor = valor;
	delta->pai = NULL;
	delta->esquerdo = NULL;
	delta->direito = NULL;
	delta->cor = 1;
	
	return delta;
}

void treeInsert(arvore *tree, int valor){
	node *alfa, *beta, *delta;	
	alfa = tree->raiz;
	beta = NULL;
	delta = criarno(valor);
	while(alfa != NULL){
		beta = alfa;
		if(delta->Valor < alfa->Valor){
			alfa = alfa->esquerdo;
		}else {
			alfa = alfa->direito;     
		}
	}
	delta->pai = beta;
	if(beta == NULL){
		tree->raiz = delta;
		tree->raiz->cor = 0;
			
	}else if(beta->Valor > delta->Valor){
		beta->esquerdo = delta;
	}else{
		beta->direito = delta;
	}

	Balancear(tree, delta);
}



node* criarTio(int valor){
   node *delta = (node *)malloc(sizeof(node));
	delta->Valor = valor;
	delta->pai = NULL;
	delta->esquerdo = NULL;
	delta->direito = NULL;
	delta->cor = 0;
	
	return delta;
}


node* tio(node *delta){
   if(delta->pai != NULL){
      if(delta->pai->pai != NULL){
         if(delta->pai->pai->direito != delta->pai){
            if(delta->pai->pai->direito != NULL){
               return delta->pai->pai->direito;
            }else{
               return criarTio(0);
            }
         }else{
            if(delta->pai->pai->esquerdo != NULL){
               return delta->pai->pai->esquerdo;
            }else{
               return criarTio(0);
            }
         }
      }
   }
   return criarno(0);   
}

node* treeSearch(arvore *tree, int valor){
	node* alfa;
	
	alfa = tree->raiz;
	while(alfa != NULL && alfa->Valor != valor){
		if(alfa->Valor > valor){
			alfa = alfa->esquerdo;
		}else{
			alfa = alfa->direito;
		}
	}
	
	if(alfa == NULL){
		return NULL;
	}else{
		return alfa;
	}
}

node* treeMinimum(node *alfa){
	while(alfa->esquerdo != NULL && alfa != NULL){
		alfa = alfa->esquerdo;
	}
	
	return alfa;
}

node* treeSucessor(node *alfa){
	node *beta;
	if(alfa->direito != NULL){
		return treeMinimum(alfa->direito);
	}
	
	beta = alfa->pai;
	while(beta != NULL && alfa == beta->direito){
		alfa = beta;
		beta = beta->pai;
	}	
	return beta;
}

node* treeAntecessor(node *alfa){
   node *beta;
   if(alfa->esquerdo != NULL){
      return treeMaximo(alfa->esquerdo);
      
   }
   
   beta = alfa->pai;
   while(beta != NULL && alfa == beta->esquerdo){
      alfa = beta;
      beta = beta->pai;
   }
   return beta;

}

node* treeMaximo(node *alfa){
   while(alfa->direito != NULL && alfa != NULL){
      alfa = alfa->direito;
   }
   return alfa;

}

int maximo(int esquerdo, int direito){

	if(esquerdo > direito){
		return esquerdo;

	}else{
		return direito;
	}
}


int calcular_altura(node *alfa){
	int h, altura_esquerdo, altura_direito;
	
	if (alfa == NULL){
		return 0;
	}
	else{
      altura_direito = calcular_altura(alfa->direito);
      altura_esquerdo = calcular_altura(alfa->esquerdo);	
		h = maximo(altura_esquerdo, altura_direito);
		
		return h+1;
	}
}

void rotacao_direito(node *alfa){
	node *beta = alfa->esquerdo;
	node *y_direito = beta->direito;
	
	beta->pai = alfa->pai;
	if(alfa->pai != NULL){
	   if(alfa == alfa->pai->direito){
		   alfa->pai->direito = beta;
		}else{
		   alfa->pai->esquerdo = beta;
		}
	}
	beta->direito = alfa;
	alfa->pai = beta;
	alfa->esquerdo = y_direito;
	if(y_direito != NULL){
		y_direito->pai = alfa;
	}
	
}

void rotacao_esquerdo(node *alfa){
	
	node *beta = alfa->direito;
	node *y_esquerdo = beta->esquerdo;
	
	beta->pai = alfa->pai;
	if(alfa->pai != NULL){
	   if(alfa == alfa->pai->direito){
		   alfa->pai->direito = beta;
		}else{
		   alfa->pai->esquerdo = beta;}}
	beta->esquerdo = alfa;
	alfa->pai = beta;
	alfa->direito = y_esquerdo;
	if(y_esquerdo != NULL){
		y_esquerdo->pai = alfa;}}


char* cor(node *a){
	if(a->cor == 0){
		return "N";
	}else{
		return "R";
	}
}

void salvarArquivo(node *a, FILE *arquivo){
	
	fprintf(arquivo, "(%d%s ", a->Valor, cor(a));
	if(a->esquerdo == NULL){
		if(a->direito == NULL){
			fprintf(arquivo, "(alfa)");
		}else{
			fprintf(arquivo, "(alfa");
		}
			
	}else{
		salvarArquivo(a->esquerdo, arquivo);
	
	}
	
	if(a->direito == NULL){
		if(a->esquerdo == NULL){
			fprintf(arquivo, " (alfa)");
		}else{
			fprintf(arquivo, "alfa)");
		}
	}else{
		salvarArquivo(a->direito, arquivo);
	}
	
	fprintf(arquivo, ")");
	
}

void lerArquivo(arvore *tree, FILE *arquivo){
	int i;
	
	
	while(fscanf(arquivo, "%d", &i) != EOF){
		treeInsert(tree, i);
	}

}

void help(char *name){
	printf("-h                     : Help\n");
	printf("-o <arquivo>           : Arquivo de Saida\n");
	printf("-f <arquivo>           : Arquivo de Entrada\n");
	printf("-m                     : Menor Elemento\n");
	printf("-M                     : Maior Elemento\n");
	printf("-a <elemento>          : Antecessor\n");
	printf("-s <elemento>          : Sucessor\n");
	exit(-1);
}

void exception(char *name){
	printf("Parāmetros com conflitos \n");
	exit(-1);
}


