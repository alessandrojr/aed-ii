#ifndef _RN_H
#define _RN_H

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct node{
	struct node *pai;
	struct node *esquerdo;
	struct node *direito;
	int cor;	
	int Valor;

}node;

typedef struct arvore{
	node *raiz;

}arvore;

node* tio(node *delta);
node* criarTio(int valor);
void init(arvore *inicio);
void percursoEmOrdem(node *alfa);
void percursoPreOrdem(node *alfa);
void percursoPosOrdem(node *alfa);
node* criarno(int valor);
void treeInsert(arvore *tree, int valor);
int calcular_altura(node *alfa);
int maximo(int esquerdo, int direito);
void balancear(arvore *tree, node *delta);
void rotacao_direito(node *alfa);
void rotacao_esquerdo(node *alfa);
node* treeSearch(arvore *tree, int valor);
node* treeMinimum(node *alfa);
node* treeSucessor(node *alfa);
node* treeAntecessor(node *alfa);
node* treeMaximo(node *alfa);
void salvarArquivo(node *a, FILE *arquivo);
void lerArquivo(arvore *tree, FILE *arquivo);
char* cor(node *a);
void help(char *name);
void exception(char *name);
#endif

